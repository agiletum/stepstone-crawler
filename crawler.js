#!/usr/bin/env node
'use strict';

const fetch = require('node-fetch');
const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const { Pool } = require('pg')
const client = new Pool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PW,
    database: 'agile',
    port: 5432
});

var ids2crawl = [];
var errors = 0;
var offset = Number.parseInt(process.env.OFFSET) || 0;

const iterate = () => {
    if (ids2crawl.length == 0) {
        fetch(`https://www.stepstone.de/5/ergebnisliste.html?*mf=a&ke=agile&li=100&of=${offset}`, {
            headers: { "User-Agent": "Mozilla/5.0 (X11; Linux; rv:74.0) Gecko/20100101 Firefox/74.0" }
        }).then(r => r.text()).then(c => {
            const dom = new JSDOM(c);
            console.log(dom.window.document.title);
            console.log(dom.window.document.getElementsByTagName("article").length);
            dom.window.document.querySelectorAll("article").forEach(ele => ids2crawl.push(ele.id.substring(9)));
            offset += 100;
            console.log(`Fetched ${ids2crawl.length} new job offers, next offset: ${offset}`);
        });
    }
    const id = ids2crawl.pop();
    if (id) {
        console.log(`Query for ${id}, ${ids2crawl.length} in current set, offset = ${offset}`);
        fetch(`https://www.stepstone.de/stellenangebote--Scrum-Master-m-w-d-Videointerview-moeglich-Karlsruhe-Fiducia-GAD-IT-AG--${id}-inline.html`,
            { headers: { "User-Agent": "Mozilla/5.0 (X11; Linux; rv:74.0) Gecko/20100101 Firefox/74.0" } })
            .then(r => r.text()).then(c => {
                const dom = new JSDOM(c);
                if (dom.window.document.querySelector(".at-section-text-profile-content > ul")) {
                    const childs = dom.window.document.querySelector(".at-section-text-profile-content > ul").children;
                    const skills = new Array(childs.length).fill().map((_, i) => childs.item(i)).map(({ textContent }) => textContent);
                    const description = dom.window.document.querySelector(".at-section-text-description").textContent;
                    var header = "Unbekannt";
                    try {
                        header = dom.window.document.querySelector(".at-listing-nav-listing-title").textContent
                    } catch (ex) {
                        header = dom.window.document.querySelector(".at-header-company-jobTitle").textContent
                    }
                    console.log("joboffer: " + header);
                    var date = null;
                    try {
                        date = dom.window.document.querySelector(".date-time-ago").getAttribute("data-date");
                    } catch (ex) {
                        try {
                            date = dom.window.document.getElementsByTagName("time")[0].getAttribute("datetime");
                        } catch (ex) {

                        }
                    }
                    var location;
                    try {
                        location = dom.window.document.querySelectorAll(".at-listing__list-icons_location")[0].textContent;
                    } catch (ex) {
                        location = "Unbekannt";
                    }
                    var companysector = "Unbekannt";
                    var companyname = "Unbekannt";
                    var jobcount = -1;
                    try {
                        const { sectors, jobsCount, name } = JSON.parse(dom.window.document.querySelector("div[data-block='app-companyCard']").getAttribute("data-initialdata"));
                        companyname = name;
                        jobcount = jobsCount;
                        companysector = sectors[0].name;
                    } catch (ex) {
                    }
                    var v = {
                        "jobid": id,
                        "datasource": "stepstone.de",
                        "jobdescription": "DESCRIPTION\n" + description + "\nSKILLS\n" + skills.join("\n"),
                        "companyname": companyname,
                        "companyheader": header,
                        "companyindustry": companysector,
                        "joblocationcountry": "Germany",
                        "joblocationcity": location,
                        "posteddate": date
                    };
                    console.log(`${header} // ${companyname} // ${companysector}\nDate: ${date}\nLocation: ${location.trim()}\nSkills: ${skills.length}`);
                    var keys = Object.keys(v);
                    const query = "INSERT INTO data (" + keys.join(" ,") + ") VALUES(" + keys.map((_, i) => "$" + (i + 1)).join(" ,") + ")";
                    client.query(query, keys.map(key => v[key]), (err) => {
                        console.log(err ? err.stack : "saved to db")
                        if (err) {
                            errors++;
                        } else {
                            errors = 0;
                        }
                    });
                } else {
                    const ele = dom.window.document.querySelector(".alert-warning");
                    if (!ele || !ele.textContent.trim().startsWith("Diese Stellenanzeige ist nicht mehr verfügbar.")) {
                        console.warn(`>> INVALID RESULT FOR ${id}`);
                        errors++;
                    } else {
                        console.log(`>> Stellenanzeige ${id} ist nicht mehr verfügbar.`);
                    }
                }
            }).catch((e) => console.error(e));
    } else {
        errors++;
    }
    if (errors < 7) setTimeout(() => iterate(), 10000);
}
iterate();